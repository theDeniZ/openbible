//
//  ModuleTests.swift
//  OpenBibleTests
//
//  Created by Dobanda, Denis on 17.10.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import XCTest
@testable import OpenBible
import CoreData.NSManagedObjectContext

class ModuleTests: XCTestCase {

    func testCoreData() {
        let context = AppDelegate.shared.persistentContainer.viewContext
        
        do {
            try CoreManager.shared.loadCoreIfNeeded(to: context)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    func testKeys() {
        let context = AppDelegate.shared.persistentContainer.viewContext
        
        let all = CoreModule.availableKeys(in: context)
        print(all)
        XCTAssert(all.count > 1)
    }
    
    func testKeysAndNames() {
        let context = AppDelegate.shared.persistentContainer.viewContext
        
        let all = CoreModule.availableKeysAndNames(in: context)
        print(all)
        XCTAssert(all.count > 1)
    }
}
