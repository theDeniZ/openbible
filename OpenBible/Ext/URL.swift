//
//  URL.swift
//  OpenBible
//
//  Created by Dobanda, Denis on 10.11.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import Foundation

extension URL {
    static var documents: URL {
        FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
    
    static func documents(file name: StringLiteralType) -> URL {
        documents.appendingPathComponent(name)
    }
}
