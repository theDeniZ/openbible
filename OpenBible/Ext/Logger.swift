//
//  Logger.swift
//  OpenBible
//
//  Created by Dobanda, Denis on 10.11.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import Foundation
import XCGLogger

let log: XCGLogger = {
    let log = XCGLogger.default
    
    // Create a destination for the system console log (via NSLog)
//    let systemDestination = AppleSystemLogDestination(identifier: "advancedLogger.systemDestination")
    let systemDestination = ConsoleDestination(identifier: "advancedLogger.systemDestination")
    
    // Optionally set some configuration options
    systemDestination.outputLevel = .verbose
    systemDestination.showLogIdentifier = false
    systemDestination.showFunctionName = true
    systemDestination.showThreadName = true
    systemDestination.showLevel = true
    systemDestination.showFileName = true
    systemDestination.showLineNumber = true
    systemDestination.showDate = false
    
    // Add the destination to the logger
    log.add(destination: systemDestination)
    
//    // Create a file log destination
//    let fileDestination = FileDestination(writeToFile: URL.documents(file: "log.log"), identifier: "advancedLogger.fileDestination")
//
//    // Optionally set some configuration options
//    fileDestination.outputLevel = .debug
//    fileDestination.showLogIdentifier = false
//    fileDestination.showFunctionName = true
//    fileDestination.showThreadName = true
//    fileDestination.showLevel = true
//    fileDestination.showFileName = true
//    fileDestination.showLineNumber = true
//    fileDestination.showDate = true
//
//    // Process this destination in the background
//    fileDestination.logQueue = XCGLogger.logQueue
//
//    // Add the destination to the logger
//    log.add(destination: fileDestination)
    
    // Add basic app info, version info etc, to the start of the logs
    log.logAppDetails()
    
    return log
}()
