//
//  StoreModel.swift
//  OpenBible
//
//  Created by Dobanda, Denis on 17.10.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import Foundation
import CoreData

extension CodingUserInfoKey {
    static let context = CodingUserInfoKey(rawValue: "context")
}

class CoreVerse: NSManagedObject, Decodable {

    enum CodingKeys: String, CodingKey {
        case number, text
    }
    
    required convenience init(from decoder: Decoder) throws {
        guard let context = decoder.userInfo[CodingUserInfoKey.context!] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: String(describing: Self.self), in: context)
        else { fatalError() }
        
        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.number = try container.decode(Int32.self, forKey: .number)
        self.text = try container.decode(String.self, forKey: .text)
    }
    
    static func get(number: Int, chapter: Int, book: Int, module: String, context: NSManagedObjectContext) -> CoreVerse? {
        let request: NSFetchRequest<CoreVerse> = fetchRequest()
        request.predicate = NSPredicate(format: "number = %@ AND chapter.number = %@ AND chapter.book.number = %@ AND chapter.book.module.key = %@", argumentArray: [number, chapter, book, module])
        
        return try? context.fetch(request).first
    }
}

class CoreChapter: NSManagedObject, Decodable {
    
    enum CodingKeys: String, CodingKey {
        case number, verses
    }
    
    required convenience init(from decoder: Decoder) throws {
        guard let context = decoder.userInfo[CodingUserInfoKey.context!] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: String(describing: Self.self), in: context)
            else { fatalError() }
        
        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.number = try container.decode(Int32.self, forKey: .number)
        let verses = try container.decode([CoreVerse].self, forKey: .verses)
        for verse in verses { verse.chapter = self }
        self.verses = NSSet(array: verses)
    }
}

class CoreBook: NSManagedObject, Decodable {
    
    enum CodingKeys: String, CodingKey {
        case name, number, chapters
    }
    
    required convenience init(from decoder: Decoder) throws {
        guard let context = decoder.userInfo[CodingUserInfoKey.context!] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: String(describing: Self.self), in: context)
            else { fatalError() }
        
        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try? container.decode(String.self, forKey: .name)
        self.number = try container.decode(Int32.self, forKey: .number)
        let chapters = try container.decode([CoreChapter].self, forKey: .chapters)
        for chapter in chapters { chapter.book = self }
        self.chapters = NSSet(array: chapters)
    }
    
    static func get(by name: String, from context: NSManagedObjectContext) -> CoreBook? {
        let request: NSFetchRequest<CoreBook> = CoreBook.fetchRequest()
        request.predicate = NSPredicate(format: "name LIKE %@", name)
        return try? context.fetch(request).first
    }
}

struct ModuleKey: Identifiable, Hashable, Codable {
    
    var id: String { key }
    let key: String
    let name: String?
    
    init(key: String) {
        self.key = key
        self.name = nil
    }
    
    init(key: String, name: String? = nil) {
        self.key = key
        self.name = name
    }
    
    static func == (_ lhs: ModuleKey, _ rhs: ModuleKey) -> Bool {
        lhs.key == rhs.key
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(key)
    }
    
}

class CoreModule: NSManagedObject, Decodable {
    
    enum CodingKeys: String, CodingKey {
        case key, name, books
    }
    
    required convenience init(from decoder: Decoder) throws {
        guard let context = decoder.userInfo[CodingUserInfoKey.context!] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: String(describing: Self.self), in: context)
            else { fatalError() }
        
        self.init(entity: entity, insertInto: context)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try? container.decode(String.self, forKey: .name)
        self.key = try container.decode(String.self, forKey: .key)
        let books = try container.decode([CoreBook].self, forKey: .books)
        for book in books { book.module = self }
        self.books = NSSet(array: books)
    }
    
    static func all(from context: NSManagedObjectContext) -> [CoreModule] {
        (try? context.fetch(CoreModule.fetchRequest())) ?? []
    }
    
    static func with(key: String, from context: NSManagedObjectContext) -> CoreModule? {
        let request: NSFetchRequest<CoreModule> = Self.fetchRequest()
            .with(predicate: NSPredicate(format: "key = %@", argumentArray: [key]))
        return try? context.fetch(request).first
    }
    
    static func availableKeys(in context: NSManagedObjectContext) -> [ModuleKey] {
        let request: NSFetchRequest<CoreModule> = Self.fetchRequest()
        return ((try? context.fetch(request))?.compactMap { $0.key! } ?? []).map(ModuleKey.init)
    }
    
    static func availableKeysAndNames(in context: NSManagedObjectContext) -> [ModuleKey] {
        let request: NSFetchRequest<CoreModule> = Self.fetchRequest()
        return ((try? context.fetch(request))?.compactMap { ($0.key!, $0.name) } ?? []).map { ModuleKey(key: $0.0, name: $0.1) }
    }
}

extension NSFetchRequest {
    @objc func with(predicate: NSPredicate) -> Self {
        self.predicate = predicate
        return self
    }
}
