//
//  CoreModel.swift
//  OpenBible
//
//  Created by Denis Dobanda on 06.10.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import Foundation
import Combine

enum Defaults {
    static func clearEverythingIfNeeded() {
        guard let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String else { return }
        if let current = UserDefaults().string(forKey: "CoreDataBuildVersion"), current != build {
            try? Settings(loading: false).save()
            try? History().save()
            log.info("Cleared defaults")
        }
        UserDefaults().set(build, forKey: "CoreDataBuildVersion")
    }
}

struct PickerSelection: Hashable, Codable {
    var selected: [ModuleKey]
    var available: [ModuleKey]
}

class Settings: ObservableObject, Codable {
    
    private static let userDefaultsKey = "user-settings"
    
    static let shared = Settings()
    
    @Published var showsColumnsInPortrait = false
    @Published var columnsInPortrait = PickerSelection(selected: [ModuleKey(key: "kjv")], available: [])
    
    @Published var showsColumnsInLandscape = false
    @Published var columnsInLandscape = PickerSelection(selected: [ModuleKey(key: "kjv")], available: [])
    
    @Published var fontName = FontNames.Helvetica
    @Published var fontSize = FontSize.normal
    
    @Published var strongNumbers = true
    
    @Published var currentBibleIndices: [BibleIndex] = [BibleIndex(book: 1, chapter: 1, verses: [1, 2, 3])]

    enum CodingKeys: String, CodingKey {
        case columnsInPortrait, showsColumnsInPortrait,
        fontName, fontSize, strongNumbers,
        showsColumnsInLandscape, columnsInLandscape,
        currentBibleIndices
    }
    
    init(loading: Bool = true) {
        guard let data = UserDefaults.standard.data(forKey: Settings.userDefaultsKey) else { return }
        if loading, let coded = try? JSONDecoder().decode(Settings.self, from: data) {
            showsColumnsInPortrait = coded.showsColumnsInPortrait
            columnsInPortrait = coded.columnsInPortrait
            showsColumnsInLandscape = coded.showsColumnsInLandscape
            columnsInLandscape = coded.columnsInLandscape
            fontName = coded.fontName
            fontSize = coded.fontSize
            strongNumbers = coded.strongNumbers
            currentBibleIndices = coded.currentBibleIndices
        }
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        showsColumnsInPortrait = try container.decode(Bool.self, forKey: .showsColumnsInPortrait)
        columnsInPortrait = try container.decode(PickerSelection.self, forKey: .columnsInPortrait)
        columnsInPortrait.available = CoreModule.availableKeysAndNames(in: AppDelegate.shared.persistentContainer.viewContext)
            .map { ModuleKey(key: $0.key, name: $0.name) }
            .filter { !columnsInPortrait.selected.contains($0) }
        
        showsColumnsInLandscape = try container.decode(Bool.self, forKey: .showsColumnsInLandscape)
        columnsInLandscape = try container.decode(PickerSelection.self, forKey: .columnsInLandscape)
        
        fontName = try container.decode(FontNames.self, forKey: .fontName)
        fontSize = try container.decode(FontSize.self, forKey: .fontSize)
        
        strongNumbers = try container.decode(Bool.self, forKey: .strongNumbers)
        
        currentBibleIndices = (try? container.decode([BibleIndex].self, forKey: .currentBibleIndices)) ?? []
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(showsColumnsInPortrait, forKey: .showsColumnsInPortrait)
        try container.encode(columnsInPortrait, forKey: .columnsInPortrait)
        
        try container.encode(showsColumnsInLandscape, forKey: .showsColumnsInLandscape)
        try container.encode(columnsInLandscape, forKey: .columnsInLandscape)
        
        try container.encode(fontName, forKey: .fontName)
        try container.encode(fontSize, forKey: .fontSize)
        
        try container.encode(strongNumbers, forKey: .strongNumbers)
        
        try container.encode(currentBibleIndices, forKey: .currentBibleIndices)
    }
    
    open func save() throws {
        let data = try JSONEncoder().encode(self)
        UserDefaults.standard.set(data, forKey: Settings.userDefaultsKey)
    }
}

class History: ObservableObject, Codable {
    
    private static let userDefaultsKey = "user-history"
    
    @Published var searches: [String]
    
    static let shared = UserDefaults.standard.data(forKey: History.userDefaultsKey).flatMap { try? JSONDecoder().decode(History.self, from: $0) } ?? History()
    
    enum CodingKeys: String, CodingKey {
        case searches
    }
    
    init() {
        searches = []
    }
    
    required init(from decoder: Decoder) throws {
        let contaienr = try decoder.container(keyedBy: CodingKeys.self)
        searches = try contaienr.decode([String].self, forKey: .searches)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(searches, forKey: .searches)
    }
    
    open func add(search: String) {
        searches.insert(search, at: 0)
        while searches.count > 20 {
            _ = searches.popLast()
        }
    }
    
    final func save() throws {
        UserDefaults.standard.set(try JSONEncoder().encode(self), forKey: History.userDefaultsKey)
    }
}

struct BibleIndex: Codable, Identifiable, CustomStringConvertible {
    var id = UUID()
    var book, chapter: Int
    var verses: [Int] = []
    
    var description: String {
        "Book \(book). `fatalError()`"
    }
}

class Favourites {
    
    private static let userDefaultsKey = "user-favourites"
    
    private(set) var indices: [BibleIndex] = []
    
    static let shared = Favourites()
    
    init() {
        if let data = UserDefaults.standard.data(forKey: Favourites.userDefaultsKey) {
            let indices = try! JSONDecoder().decode([BibleIndex].self, from: data)
            self.indices = indices
        }
    }
    
    func add(_ index: BibleIndex) {
        indices.append(index)
        if let data = try? JSONEncoder().encode(indices) {
            UserDefaults.standard.set(data, forKey: Favourites.userDefaultsKey)
        }
    }
    
}

class BibleIndexManager {
    
    enum Errors: Error {
        case notFound
        case wrongFormat(String)
    }
    
    static let shared = BibleIndexManager()
    
    func parse(search string: String, indicesDivider: String = "/", verseFromChapterDivider: String = "v", betweenVerseDivider: String = ".") throws -> [BibleIndex] {
        
        func parse(verse notation: String) throws -> [Int] {
            if notation.contains("-") {
                let parts = notation.components(separatedBy: "-")
                guard parts.count == 2 else { throw Errors.wrongFormat("The verse range should be '(d+)-(d+)' of regex, found \(notation) instead") }
                let first = Int(parts[0])!
                let second = Int(parts[1])!
                if first <= second {
                    return (first ... second).compactMap { $0 }
                } else {
                    return (second ... first).compactMap { $0 }
                }
            } else {
                guard notation.matches(#"\d+"#) else { throw Errors.wrongFormat("The verse itself should be '(d+)', found \(notation) instead") }
                return [Int(notation)!]
            }
        }
        
        func parse(_ search: String) throws -> BibleIndex {
            let parts = search.components(separatedBy: verseFromChapterDivider)
            guard parts.count == 2 else { throw Errors.wrongFormat("Should have a 'v' divider between chapter and verses.") }
            guard parts.first!.matches(#"^(.*)(\d+)$"#) else { throw Errors.wrongFormat("Should have a proper book&chapter format") }
            guard parts.last!.matches(#"^([.-]?\d+)+$"#) else { throw Errors.wrongFormat("Should have a proper verse format") }
            let match = parts.first!.capturedGroups(withRegex: #"^(.*)(\d+)$"#)!
            let book = match[0]
            let chapter = Int(match[1])!
            
            let versesOfString = parts.last!.components(separatedBy: betweenVerseDivider)
            let verses = try versesOfString.flatMap(parse(verse:))
            
            let coreBook = CoreBook.get(by: book, from: AppDelegate.shared.persistentContainer.viewContext)
            
            guard coreBook != nil else { throw Errors.notFound }
            
            return BibleIndex(book: Int(coreBook!.number), chapter: chapter, verses: verses)
        }
        
        return try string.components(separatedBy: indicesDivider).map(parse)
    }
}
