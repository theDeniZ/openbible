//
//  SampleModel.swift
//  OpenBible
//
//  Created by Denis Dobanda on 06.10.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import Foundation

// swiftlint:disable:next line_length
let someVerse: String = "Some verse goes here with a pretty large amount of words describing some things (or, if you preffer, it could be nothing at all), but in reality it is done just to test things around with some custom fonts and multiple verses. So, yea, like that!"
let someVerse2 = "Some verse goes here with a pretty large amount of words describing some things (or, if you preffer, it could be nothing at all)."

let somePage = Page(
    title: "KJV",
    sections: [
        SectionOfVerses(
            title: "First",
            verseLines: [
                VerseLine(
                    verses: [
                        Verse(text: someVerse),
                        Verse(text: someVerse2)
                    ]
                ),
                VerseLine(
                    verses: [
                        Verse(text: someVerse2),
                        Verse(text: someVerse2)
                    ]
                ),
                VerseLine(
                    verses: [
                        Verse(text: someVerse),
                        Verse(text: someVerse2)
                    ]
                )
            ]
        ),
        SectionOfVerses(
            title: "Second",
            verseLines: [
                VerseLine(
                    verses: [
                        Verse(text: someVerse),
                        Verse(text: someVerse2)
                    ]
                ),
                VerseLine(
                    verses: [
                        Verse(text: someVerse2),
                        Verse(text: someVerse2)
                    ]
                ),
                VerseLine(
                    verses: [
                        Verse(text: someVerse),
                        Verse(text: someVerse2)
                    ]
                )
            ]
        ),
        SectionOfVerses(
            title: "Third",
            verseLines: [
                VerseLine(
                    verses: [
                        Verse(text: someVerse),
                        Verse(text: someVerse2)
                    ]
                ),
                VerseLine(
                    verses: [
                        Verse(text: someVerse2),
                        Verse(text: someVerse2)
                    ]
                ),
                VerseLine(
                    verses: [
                        Verse(text: someVerse),
                        Verse(text: someVerse2)
                    ]
                )
            ]
        )
    ]
)

let downloadItems: [DownloadItem] = [
    .init(title: "King James Version", size: 1203, loaded: true),
    .init(title: "King James Version", size: 1204, loaded: false),
    .init(title: "King James Version", size: 1205, loaded: false),
    .init(title: "King James Version", size: 1206, loaded: true),
    .init(title: "King James Version", size: 1207, loaded: false),
    .init(title: "King James Version", size: 1208, loaded: true),
    .init(title: "King James Version", size: 1209, loaded: false),
    .init(title: "King James Version", size: 1210, loaded: false)
]

let downloadList = DownloadList(sections: [DownloadSection(list: downloadItems, title: "Modules"), DownloadSection(list: downloadItems, title: "Strong numbers")])
