//
//  PageModel.swift
//  OpenBible
//
//  Created by Dobanda, Denis on 10.11.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import Foundation

struct Verse: Identifiable {
    var id = UUID()
    var text: String
}

struct VerseLine: Identifiable {
    var id = UUID()
    var verses: [Verse]
}

struct SectionOfVerses: Identifiable {
    var id = UUID()
    var title: String
    var verseLines: [VerseLine]
}

struct Page: Identifiable {
    var id = UUID()
    var title: String
    var sections: [SectionOfVerses]
    
    mutating func next(using sectionIndex: Int) {
        
    }
}
