//
//  DownloadModel.swift
//  OpenBible
//
//  Created by Dobanda, Denis on 10.11.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import Foundation

class DownloadItem: ObservableObject, Identifiable {
    
    var id = UUID()
    var title: String
    var size: UInt
    @Published var loaded: Bool
    @Published var loading: Bool = false
    @Published var loadingProgress: Int = 0
    
    init(title: String, size: UInt, loaded: Bool = false) {
        self.title = title
        self.size = size
        self.loaded = loaded
    }
}

class DownloadSection: Identifiable {
    var id = UUID()
    var list: [DownloadItem]
    var title: String?
    
    init(list: [DownloadItem], title: String? = nil) {
        self.list = list
        self.title = title
    }
}

class DownloadList: Identifiable {
    var id = UUID()
    var sections: [DownloadSection]
    
    init(sections: [DownloadSection]) {
        self.sections = sections
    }
}
