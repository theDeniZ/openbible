//
//  Constants.swift
//  OpenBible
//
//  Created by Dobanda, Denis on 15.10.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import Foundation
import SwiftUI

enum FontSize: Int, CaseIterable, Codable {
    case small = 12
    case smaller = 14
    case normal = 16
    case bigger = 18
    case big = 20
    case biggest = 24
    
    static var current: FontSize {
        Settings.shared.fontSize
    }
}

enum FontNames: String, CaseIterable, Codable {
    case Helvetica
    case Arial
    
    static var current: FontNames {
        Settings.shared.fontName
    }
    
    var font: Font {
        font(of: FontSize.current)
    }

    func font(of size: FontSize) -> Font {
        switch self {
        case .Helvetica:
            return Font.custom("Helvetica", size: CGFloat(size.rawValue))
        case .Arial:
            return Font.custom("Arial", size: CGFloat(size.rawValue))
        }
    }
}
