//
//  CoreManager.swift
//  OpenBible
//
//  Created by Dobanda, Denis on 18.10.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import Foundation
import CoreData.NSManagedObjectContext

struct CoreManager {
    
    static let shared = CoreManager(context: AppDelegate.context)
    
    let context: NSManagedObjectContext
    
    // MARK: Loading
    
    func loadCoreIfNeeded(to context: NSManagedObjectContext) throws {
        let allModulesCount = 6
        let all = CoreModule.all(from: context)
        guard all.count != allModulesCount else { return }
        
        log.verbose("Loading Core becouse needed")
        
        for mod in all { context.delete(mod) }
        
        for index in 0 ..< allModulesCount {
            if let url = Bundle.main.url(forResource: "export\(index)", withExtension: "json"),
                let data = try? Data(contentsOf: url) {
                let decoder = JSONDecoder()
                decoder.userInfo[CodingUserInfoKey.context!] = context
                _ = try decoder.decode(CoreModule.self, from: data)
            }
        }
        
        try context.save()
    }
    
    // MARK: Getting
    
    var currentPage: Page {
        let modules: [ModuleKey] = Settings.shared.columnsInPortrait.selected
        let indices: [BibleIndex] = Settings.shared.currentBibleIndices
        
        let sections: [SectionOfVerses] = indices.map { index in
            var verseNumbers = index.verses
            if verseNumbers.isEmpty {
                verseNumbers = Array(0 ..< modules.map { versesCount(at: index, of: $0) }.max()!)
            }
            let lines = verseNumbers.map { verseNr in
                VerseLine(verses: modules.map { module in
                    Verse(text: verse(at: index, number: verseNr, module: module).flatMap { "\(verseNr) \($0.text ?? "")" } ?? "" )
                })
            }
            return SectionOfVerses(title: index.description, verseLines: lines)
        }
        
        return Page(title: "Read", sections: sections)
    }
    
    func next(after page: inout Page, using index: Int) {
        // TODO: DO!
    }
    
    // MARK: Private Helpers
    
    private func allVerses(at index: BibleIndex, of module: ModuleKey) -> [CoreVerse] {
        guard let module = CoreModule.with(key: module.key, from: context),
            let book = (module.books?.allObjects as? [CoreBook])?.first(where: { $0.number == index.book }),
            let chapter = (book.chapters?.allObjects as? [CoreChapter])?.first(where: { $0.number == index.chapter })
        else { return [] }
        return (chapter.verses?.allObjects as? [CoreVerse])?.sorted { $0.number < $1.number } ?? []
        
    }
    
    private func versesCount(at index: BibleIndex, of module: ModuleKey) -> Int {
        guard let module = CoreModule.with(key: module.key, from: context),
            let book = (module.books?.allObjects as? [CoreBook])?.first(where: { $0.number == index.book }),
            let chapter = (book.chapters?.allObjects as? [CoreChapter])?.first(where: { $0.number == index.chapter })
        else { return 0 }
        return chapter.verses?.count ?? 0
    }
    
    private func verse(at index: BibleIndex, number: Int, module: ModuleKey) -> CoreVerse? {
        CoreVerse.get(number: number, chapter: index.chapter, book: index.book, module: module.key, context: context)
    }
}
