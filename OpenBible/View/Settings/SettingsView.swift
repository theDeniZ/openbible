//
//  SettingsView.swift
//  OpenBible
//
//  Created by Denis Dobanda on 13.10.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import SwiftUI
import Combine

struct SettingsView: View {
    
    @ObservedObject private(set) var settings = Settings.shared
    
    @State private var showsSaveMessage = false
    @State private var saveMessage: String?
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    Toggle(isOn: $settings.strongNumbers) {
                        Text("Show strong numbers")
                    }
                }

                Section {
                    Picker("Font", selection: $settings.fontName) {
                        ForEach(FontNames.allCases, id: \.self) {
                            Text($0.rawValue).font($0.font)
                        }
                    }
                    Picker("Font Size", selection: $settings.fontSize) {
                        ForEach(FontSize.allCases, id: \.self) { size in
                            Text("\(size.rawValue)").font(FontNames.current.font(of: size))
                        }
                    }
                }
                
                SettingsColumnPickerView(
                    text: "Shows columns in Portrait",
                    showsColumns: $settings.showsColumnsInPortrait,
                    selection: $settings.columnsInPortrait
                )
                
                SettingsColumnPickerView(
                    text: "Shows columns in Landscape",
                    showsColumns: $settings.showsColumnsInLandscape,
                    selection: $settings.columnsInLandscape
                )
                
                Section {
                    Button(action: {
                        do {
                            try self.settings.save()
                            self.saveMessage = "Success!"
                            self.showsSaveMessage = true
                            Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { _ in self.showsSaveMessage = false; self.saveMessage = nil }
                        } catch {
                            self.saveMessage = "Failed!"
                            self.showsSaveMessage = true
                            Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { _ in self.showsSaveMessage = false; self.saveMessage = nil }
                        }
                    }) {
                        Text("Save")
                    }
                    if showsSaveMessage {
                        Text(saveMessage ?? "")
                    }
                }
                
            }
            .navigationBarTitle("Settings")
        }
    }
}

struct SettingsColumnPickerView: View {
    
    var text: String
    @Binding var showsColumns: Bool
    @Binding var selection: PickerSelection
    
    var body: some View {
        Section {
            Toggle(isOn: $showsColumns) {
                Text(text)
            }
            if showsColumns {
                NavigationLink("Columns",
                               destination: SortingPickerView(selection: $selection)
                                .environment(\.editMode, Binding.constant(EditMode.active))
                )
            }
        }
    }
}

struct SortingPickerView: View {
    
    @Binding var selection: PickerSelection
    
    var body: some View {
        List {
            Section(header: Text("Selected")) {
                ForEach(selection.selected) {
                    Text("\($0.key)\($0.name.flatMap { " - \($0)" } ?? "")")
                }
                .onMove(perform: move)
                .onDelete(perform: delete)
            }
            Section(header: Text("Available")) {
                ForEach(selection.available) { item in
                    HStack {
//                        Image("add").frame(width: 32, height: 32)
                        Text("\(item.key)\(item.name.flatMap { " - \($0)" } ?? "")")
                    }
                    .onTapGesture {
                        self.add(item)
                    }
                }
                
            }
        }
    }
    
    func delete(_ indexSet: IndexSet) {
        if selection.selected.count > 1, let first = indexSet.first {
            let item = selection.selected[first]
            selection.selected.remove(at: first)
            selection.available.append(item)
        }
    }
    
    func move(from indexSet: IndexSet, to index: Int) {
        self.selection.selected.move(fromOffsets: indexSet, toOffset: index)
    }
    
    func add(_ moduleKey: ModuleKey) {
        selection.available.removeAll { $0 == moduleKey }
        selection.selected.append(moduleKey)
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
