//
//  GotoView.swift
//  OpenBible
//
//  Created by Dobanda, Denis on 15.10.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import SwiftUI
import Combine

struct GotoView: View {
    
    var onSearch: ((String) -> Bool)?
    
    @Environment(\.presentationMode) var presentationMode
    
    @ObservedObject private var history = History.shared
    
    @State private var searchText: String = ""
    
    init(_ onSearch: ((String) -> Bool)? = nil) {
        self.onSearch = onSearch
    }
    
    var body: some View {
        VStack {
            TextField("Search parameter", text: $searchText) {
                self.performAction(self.searchText, fromHistory: false)
            }
            .padding()
//            Spacer()
            List(history.searches, id: \.self) { search in
                Text(search)
                .onTapGesture {
                    self.performAction(search, fromHistory: true)
                }
            }
        }
        .navigationBarTitle("Go to")
    }
    
    private func performAction(_ text: String, fromHistory: Bool) {
        if onSearch?(text) == true {
            if !fromHistory {
                history.add(search: text)
                try? history.save()
            }
            presentationMode.wrappedValue.dismiss()
        } else {
            // trigger animation?
            
        }
    }
}

struct GotoView_Previews: PreviewProvider {
    static var previews: some View {
        GotoView { text in print(text); return true }
    }
}
