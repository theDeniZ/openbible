//
//  DownloadListView.swift
//  OpenBible
//
//  Created by Denis Dobanda on 06.10.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import SwiftUI

struct DownloadListView: View {
    let list: DownloadList
    
    let loadAction: (DownloadItem) -> Void = { item in
        item.loading = true
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (_) in
            item.loadingProgress = 50
            Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (_) in
                item.loadingProgress = 90
                Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (_) in
                    item.loadingProgress = 0
                    item.loaded = true
                    item.loading = false
                }
            }
        }
    }
    
    let deleteAction: (DownloadItem) -> Void = { item in
        item.loaded = false
    }
    
    let stopAction: (DownloadItem) -> Void = { item in
        item.loading = false
        item.loadingProgress = 0
    }
    
    var body: some View {
        NavigationView {
            List {
                ForEach(list.sections) { section in
                    Section(header: SectionHeader(title: section.title)) {
                        ForEach(section.list) { item in
                            DownloadCellView(item: item) { item in
                                if item.loading {
                                    self.stopAction(item)
                                } else if item.loaded {
                                    self.deleteAction(item)
                                } else {
                                    self.loadAction(item)
                                }
                            }
                        }
                    }
                }
            }
            .navigationBarTitle("Downloads")
        }
    }
}

struct DownloadListView_Previews: PreviewProvider {
    static var previews: some View {
        DownloadListView(list: downloadList)
    }
}
