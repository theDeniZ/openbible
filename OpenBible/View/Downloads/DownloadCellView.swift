//
//  DownloadCellView.swift
//  OpenBible
//
//  Created by Denis Dobanda on 06.10.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import SwiftUI
import Combine

struct DownloadCellView: View {
    
    @ObservedObject var item: DownloadItem
    
    var action: ((DownloadItem) -> Void)?
    
    var body: some View {
        HStack {
            Text(item.title)
            Spacer()
            Text("\(item.size) Kb")
            LoadingView(model: LoadModel(loaded: item.loaded, loading: item.loading, loadingProgress: item.loadingProgress)) {
                self.action?(self.item)
            }
            .frame(width: 32, height: 32)
        }
        .padding([.bottom, .top], 5)
    }
}

struct DownloadCellView_Previews: PreviewProvider {
    static var previews: some View {
        DownloadCellView(item: downloadItems[0])
    }
}
