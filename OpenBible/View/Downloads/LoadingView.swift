//
//  LoadingView.swift
//  OpenBible
//
//  Created by Denis Dobanda on 06.10.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import SwiftUI

class LoadModel: ObservableObject {
    /// Indicated wheather the icon should be `download` on `delete`
    @Published var loaded: Bool = true
    
    /// indicated wheather the animation is ongoing
    @Published var loading: Bool = true
    
    /// The Progress from 0 to 100
    @Published var loadingProgress = 80
    
    init(loaded: Bool, loading: Bool, loadingProgress: Int) {
        self.loaded = loaded
        self.loading = loading
        self.loadingProgress = loadingProgress
    }
}

struct LoadingView: View {
    
    @ObservedObject var model: LoadModel
    
    private let deleteImageName = "delete"
    private let downloadImageName = "download"
    
    var action: (() -> Void)?
    
    var body: some View {
        ZStack {
            if self.model.loading {
                ProgressView(progress: Double(self.model.loadingProgress) / 100)
            } else {
                Image(self.model.loaded ? deleteImageName : downloadImageName)
            }
        }.onTapGesture {
            self.action?()
        }
        .aspectRatio(CGSize(width: 1, height: 1), contentMode: .fit)
    }
}

struct ProgressView: View {
    
    var progress: Double = 0
    var lineWidth: CGFloat = 3
    
    var body: some View {
        GeometryReader { proxy in
            ZStack {
                Ring(progress: self.progress)
                    .rotation(.degrees(-90))
                    .stroke(Color.blue, lineWidth: self.lineWidth)
                    .frame(width: proxy.size.width, height: proxy.size.height, alignment: .center)
                Rectangle()
                    .frame(width: proxy.size.width * 0.33, height: proxy.size.height * 0.33, alignment: .init(horizontal: .center, vertical: .center))
                    .foregroundColor(Color.blue)
            }
            .aspectRatio(proxy.size, contentMode: .fit)
        }
    }
}

struct Ring: Shape {
    
    var progress: Double = 0.0
    
    func path(in rect: CGRect) -> Path {
        
        let start = 0.0
        let end = 360 * progress
        var path = Path()
        path.addArc(
            center: CGPoint(x: rect.midX, y: rect.midY),
            radius: rect.width / 2,
            startAngle: .degrees(start),
            endAngle: .degrees(end),
            clockwise: false
        )
        
        return path
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            LoadingView(model: LoadModel(loaded: false, loading: false, loadingProgress: 0))
            LoadingView(model: LoadModel(loaded: true, loading: true, loadingProgress: 20))
            LoadingView(model: LoadModel(loaded: true, loading: true, loadingProgress: 80))
            LoadingView(model: LoadModel(loaded: true, loading: false, loadingProgress: 0))
        }
        .frame(width: 32)
    }
}
