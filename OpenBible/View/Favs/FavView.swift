//
//  FavView.swift
//  OpenBible
//
//  Created by Dobanda, Denis on 16.10.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import SwiftUI

struct FavView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    let favs = Favourites.shared
    
    var onSelect: ((BibleIndex) -> Void)?
    
    var body: some View {
        List(favs.indices) { index in
            Text("\(index.description)")
            .onTapGesture {
                self.onSelect?(index)
                self.presentationMode.wrappedValue.dismiss()
            }
        }
        .navigationBarTitle("Favourites")
    }
}

struct FavView_Previews: PreviewProvider {
    static var previews: some View {
        FavView()
    }
}
