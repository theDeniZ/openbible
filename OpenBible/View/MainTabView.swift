//
//  ContentView.swift
//  OpenBible
//
//  Created by Denis Dobanda on 20.09.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import SwiftUI

struct MainTabView: View {
    
    @State var currentTabIndex = 0
    @ObservedObject private var settings = Settings.shared
    
    var body: some View {
        TabView(selection: $currentTabIndex) {
            DownloadListView(list: downloadList)
                .tabItem {
                    Image("download")
                    Text("Download")
            }
            .tag(1)
            
            NavigationPageView(page: CoreManager.shared.currentPage)
                .tabItem {
                    Image("read")
                    Text("Read")
            }
            .tag(0)
            
            SettingsView()
                .tabItem {
                    Image("settings")
                    Text("Settings")
            }
            .tag(2)
        }
    }
}

#if DEBUG
struct MainTabView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
#endif
