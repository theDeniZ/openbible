//
//  NavigationPageView.swift
//  OpenBible
//
//  Created by Denis Dobanda on 06.10.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import SwiftUI

struct NavigationPageView: View {
    var page: Page
    
    var body: some View {
        NavigationView {
            PageView(page: page)
                .navigationBarTitle(Text(page.title), displayMode: .automatic)
                .navigationBarItems(
                    leading: NavigationLink(destination: FavView()) { Image("favourite") },
                    trailing: NavigationLink(destination: GotoView(search(phrase:))) { Image("goto") }
                )
                .gesture(
                    DragGesture(minimumDistance: 10, coordinateSpace: .local)
                        .onEnded { value in
                            log.info(value.translation)
                    }
            )
        }
    }
    
    func search(phrase: String) -> Bool {
        do {
            let indices = try BibleIndexManager.shared.parse(search: phrase)
            Settings.shared.currentBibleIndices = indices
            return true
        } catch BibleIndexManager.Errors.wrongFormat(let message) {
            print("Wrong format: \(message)")
        } catch BibleIndexManager.Errors.notFound {
            print("Index not found")
        } catch {
            print(error.localizedDescription)
        }
        return false
    }
}

#if DEBUG
struct NavigationPageView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationPageView(page: somePage)
    }
}
#endif
