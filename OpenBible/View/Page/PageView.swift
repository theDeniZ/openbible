//
//  PageView.swift
//  OpenBible
//
//  Created by Denis Dobanda on 06.10.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import SwiftUI

struct PageView: View {
    var page: Page
    
    var body: some View {
        List {
            ForEach(page.sections) { section in
//                SectionVerseView(section: section)
                Section(header: SectionHeader(title: section.title)) {
                    ForEach(section.verseLines) { line in
                        StackVerseView(verseLine: line)
                    }
                }
                .gesture(
                    DragGesture(minimumDistance: 10, coordinateSpace: .local)
                        .onEnded { value in
                            log.info(value.translation)
                    }
                )
            }
        }
    }
}

#if DEBUG
struct PageView_Previews: PreviewProvider {
    static var previews: some View {
        PageView(page: somePage)
    }
}
#endif
