//
//  StackVerseView.swift
//  OpenBible
//
//  Created by Denis Dobanda on 06.10.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import SwiftUI

struct StackVerseView: View {
    var verseLine: VerseLine
    
    var body: some View {
        HStack(alignment: .top, spacing: 20) {
            ForEach(verseLine.verses) { verse in
                VerseView(verse: verse)
            }
        }
    }
}

#if DEBUG
struct StackVerseView_Previews: PreviewProvider {
    static var previews: some View {
        StackVerseView(verseLine: somePage.sections[0].verseLines[0])
    }
}
#endif
