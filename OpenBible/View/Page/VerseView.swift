//
//  VerseView.swift
//  OpenBible
//
//  Created by Denis Dobanda on 06.10.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import SwiftUI

struct VerseView: View {
    var verse: Verse
    
    var body: some View {
        Text(verse.text)
            .lineLimit(nil)
            .multilineTextAlignment(.leading)
            .font(.subheadline)
    }
}

#if DEBUG
struct VerseView_Previews: PreviewProvider {
    static var previews: some View {
        VerseView(verse: somePage.sections[0].verseLines[0].verses[0])
    }
}
#endif
