//
//  SectionHeader.swift
//  OpenBible
//
//  Created by Denis Dobanda on 13.10.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

import SwiftUI

struct SectionHeader: View {
    
    var title: String?
    
    var body: some View {
        Text(title ?? "")
        .foregroundColor(Color.white)
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
        .background(
            LinearGradient(
                gradient: Gradient(colors: [
                    Color.clear, Color.blue, Color.clear
                ]),
                startPoint: .leading,
                endPoint: .trailing
            )
        )
    }
}

struct SectionHeader_Previews: PreviewProvider {
    static var previews: some View {
        SectionHeader()
    }
}
