//
//  SectionVerseView.swift
//  OpenBible
//
//  Created by Denis Dobanda on 06.10.19.
//  Copyright © 2019 Denis Dobanda. All rights reserved.
//

//import SwiftUI
//
//struct SectionVerseView: View {
//    var section: SectionOfVerses
//
//    var body: some View {
//        Section(header: Text(section.title)) {
//            ForEach(section.verseLines) { line in
//                StackVerseView(verseLine: line)
//            }
//        }
//    }
//}
//
//#if DEBUG
//struct SectionVerseView_Previews : PreviewProvider {
//    static var previews: some View {
//        SectionVerseView(section: somePage.sections[0])
//    }
//}
//#endif
